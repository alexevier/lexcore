#!/bin/bash

# Copyright 2024 alexevier <alexevier@proton.me>
# licensed under the zlib license <https://www.zlib.net/zlib_license.html>

target=linux
conf=debug
builddir=build

# no arguments, show help
if [ -z $1 ]; then
	$0 "help"
	exit 1
fi

# find conf and target
for var in "$@"; do
	# target
	if [ $var = 'linux' ]; then
		target=linux
	elif [ $var = 'mingw' ]; then
		target=mingw_x86_64
	elif [ $var = 'mingw_x86_64' ]; then
		target=mingw_x86_64
	elif [ $var = 'mingw_i686' ]; then
		target=mingw_i686
	fi

	# conf
	if [ $var = 'debug' ]; then
		conf=debug
	elif [ $var = 'release' ]; then
		conf=release
	fi
done

# format the complete build dir
builddir="$builddir/$target/$conf"

if [ $1 = 'run' ]; then
	echo "[run]: $target.$conf"

	$0 build $target $conf
	if [ $? != 0 ]; then
		echo "[run]: build error"
		exit 1
	fi

	if [[ $target == "mingw"* ]]; then
		wine "$builddir/test.exe"
	else
		$builddir/test
	fi

	exit 0
fi

if [ $1 = 'build' ]; then
	echo "[build]: $target.$conf"

	mkdir -p "$builddir"

	if [ ! -f "$builddir/Makefile" ]; then
		cd "$builddir"
		cmake ../../.. -DCMAKE_BUILD_TYPE=$conf -DCMAKE_TOOLCHAIN_FILE=../../../cmake/$target.cmake
		cd ../../..
	fi

	cd "$builddir"

	make
	if [ $? != 0 ]; then
		echo "[build]: error"
		exit 1
	fi

	cd ../../..

	exit 0
fi

if [ $1 = 'clean' ]; then
	if [ -z $2 ]; then
		echo "[clean]"
		rm -rf build
	else
		echo "[clean]: $target.$conf"
		rm -rf build/$target/$conf
	fi
	exit 0
fi

if [ $1 = 'help' ]; then
	echo \
"LexCore util script
Copyright (C) 2024 alexevier

usage: script.sh OPTION [target] [config]

options
    run       builds a target and runs the test
    build     build a target
    clean     clean build artifacts
    help      shows this message

targets
    linux (default)
    mingw (alias for mingw_x86_64)
    mingw_x86_64
    mingw_i686

configs
    debug
    release
"
	exit 0
fi

# reached the end, unknown option, show help
$0 help
exit 1
