/* Copyright 2024 alexevier <alexevier@proton.me>
licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#include<lexcore.h>

#include<stdio.h>

LEXCOREAPI
int main(int argc, char *argv[]){
	printf("hello world from lexcore %s!\n", lexcoreVersion());

	return 0;
}
